﻿// sklbox2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
	const int N = 8;
	int array[N][N];
	cout << "Here is your beautiful array " << N << "*" << N <<":"<< endl << endl;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			array[i][j] = i + j;
			cout << setw(5) << array[i][j];
		}
		cout << endl;
	}
	int str = 10 % N;
	cout << endl << "It is the 10th of August today. " << " 10 % " << N <<" = "<< str << endl << endl;
	cout << "Sum of numbers in the " << str << "th string = ";
	
	int sum=0;
	for (int i = 0; i < N; i++)
		sum += array[str][i];
	cout << sum << endl << endl;
}
